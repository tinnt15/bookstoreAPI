﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using BookStoreAPI.Models;
using System.Web.Http.Cors;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace BookStoreAPI.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {

        BookStoreEntities bsdb = new BookStoreEntities();

        //[AllowAnonymous]
        //public IHttpActionResult checkuser(UserData userData)
        //{
        //    string hashinput = MD5Hash(userData.password);
        //    var rs = bsdb.Users.FirstOrDefault(x => x.Email == userData.email && x.Password == hashinput);
        //    if (rs != null)
        //    {
        //        return Ok(true);

        //    }
        //    return Ok(false);
        //}


        // POST api/User/Login
        [HttpPost]
        [AllowAnonymous]
        [Route("authenticate")]
        public async Task<HttpResponseMessage> LoginUser(UserData model)
        {
            string hashinput = MD5Hash(model.password);
            // Invoke the "token" OWIN service to perform the login: /api/token
            // Ugly hack: I use a server-side HTTP POST because I cannot directly invoke the service (it is deeply hidden in the OAuthAuthorizationServerHandler class)
            var request = HttpContext.Current.Request;
            var tokenServiceUrl = request.Url.GetLeftPart(UriPartial.Authority) + request.ApplicationPath + "/api/Token";
            using (var client = new HttpClient())
            {
                var requestParams = new List<KeyValuePair<string, string>>
            {
                new KeyValuePair<string, string>("grant_type", "password"),
                new KeyValuePair<string, string>("username", model.email),
                new KeyValuePair<string, string>("password", hashinput)
            };
                var requestParamsFormUrlEncoded = new FormUrlEncodedContent(requestParams);
                var tokenServiceResponse = await client.PostAsync(tokenServiceUrl, requestParamsFormUrlEncoded);
                var responseString = await tokenServiceResponse.Content.ReadAsStringAsync();
                var responseCode = tokenServiceResponse.StatusCode;
                var responseMsg = new HttpResponseMessage(responseCode)
                {
                    Content = new StringContent(responseString, Encoding.UTF8, "application/json")
                };
                return responseMsg;
            }
        }
        public static string MD5Hash(string text)
        {
            MD5 md5 = new MD5CryptoServiceProvider();
            //compute hash from the bytes of text
            md5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(text));
            //get hash result after compute it
            byte[] result = md5.Hash;
            StringBuilder strBuilder = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                //change it into 2 hexadecimal digits
                //for each byte
                strBuilder.Append(result[i].ToString("X"));
            }
            return strBuilder.ToString();
        } 
    }
    public class UserData
    {
        public string email { get; set; }
        public string password { get; set; }
    }
}
